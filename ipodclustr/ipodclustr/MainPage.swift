//
//  MainPage.swift
//  ipodclustr
//
//  Created by ECE564 on 6/30/19.
//  Copyright © 2019 mobilecenter. All rights reserved.
//

import UIKit
import CropViewController
import os

class MainPage: UIViewController {
    
    // results from the form, to pass to master page
    var rows = 1
    var columns = 1
    var firstRowFlip: Bool = false
    var cropborder: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        os_log("ℹ️ Main menu page loaded. This is an example of additional info that may be helpful for troubleshooting.", log: .default, type: .info)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // always disable hide navbar on main screen
        self.navigationController?.hidesBarsOnTap = false
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func MasterButtonTapped(_ sender: Any) {
        let settings = MasterSettingsForms()
        self.showSpinner(onView: self.view, text: "Processing...")
        self.present(settings, animated: true)
        
        settings.onApplyCallback = { result in
            if let flip: Bool = result["flip"] as? Bool {
                self.firstRowFlip = flip
            }
            if let cropborder: Bool = result["cropborder"] as? Bool {
                self.cropborder = cropborder
            }
            
            if let rows: Int = result["rows"] as? Int, let columns: Int = result["columns"] as? Int {
                self.rows = rows
                self.columns = columns
            }
            else {
                os_log("✅ No rows or columns provided. Set again.", log: .default, type: .debug)
                self.removeSpinner()
                return
            }
            self.performSegue(withIdentifier: "MasterPageSegue", sender: nil)
            self.removeSpinner()
            return
        }
        settings.onCancelCallback = {
            self.removeSpinner()
            return
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MasterPageSegue" {
            let masterPage = segue.destination as! MasterPage
            
            masterPage.session1MaxPeerCout = self.columns  // slaves count in 1 row
            masterPage.rows = self.rows
            masterPage.columns = self.columns
            masterPage.firstRowFlip = self.firstRowFlip
            masterPage.cropborder = self.cropborder
        }
    }
}

