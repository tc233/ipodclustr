//
//  Models.swift
//  ipodclustr
//
//  Created by ECE564 on 7/15/19.
//  Copyright © 2019 mobilecenter. All rights reserved.
//

import UIKit

struct ImageWithMeta {
    var id: Int
    var delay: Int
    var originalImage: UIImage
    var croppedImageJPEGDataSet: [[Data]] = []
    var isSent: Bool = false
}
