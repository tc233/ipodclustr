//
//  SlavePage.swift
//  ipodclustr
//
//  Created by ECE564 on 6/30/19.
//  Copyright © 2019 mobilecenter. All rights reserved.
//

import UIKit
import MultipeerConnectivity
import os

class SlavePage: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    var videoView: VideoView!
    
    // keep a copy of pieces to ensure fast hit
    var ImagePieceDict: [Int: UIImage] = [:]
    var currentReceivedID: Int = 0
    
    var peerID: MCPeerID!
    var mcSession: MCSession!
    
    // no need to accept/recv invitation from other peers. just send to master.
    var serviceBrowser : MCNearbyServiceBrowser!
    
    var invitingPeerTimeout: Double = 15
    
    var connectedPeersObserver: [MCPeerID] = [] {
        didSet {
            self.popWhenMasterLeaves()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initVideoView()
        
        // Do any additional setup after loading the view.
        peerID = MCPeerID(displayName: UIDevice.current.name)
        mcSession = MCSession(peer: peerID, securityIdentity: nil, encryptionPreference: .required)
        mcSession.delegate = self
        
        self.serviceBrowser = MCNearbyServiceBrowser(peer: peerID, serviceType: globalServiceType)
        self.serviceBrowser.delegate = self
        
        self.navigationController?.hidesBarsOnTap = true
        self.navigationController?.isNavigationBarHidden = true
        
        self.joinSession(action: nil)
    }
    
    func joinSession(action: UIAlertAction?) {
        self.serviceBrowser.startBrowsingForPeers()
    }
    
    deinit {
        self.serviceBrowser.stopBrowsingForPeers()
    }
    
    override var prefersStatusBarHidden: Bool {
        return navigationController?.isNavigationBarHidden == true
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return UIStatusBarAnimation.slide
    }
    
    func initVideoView() {
        self.videoView = VideoView(frame: self.view.bounds)
        self.view.addSubview(self.videoView)
        self.videoView.isHidden = true
    }
}

extension SlavePage : MCNearbyServiceBrowserDelegate {
    
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
        os_log("❌ didNotStartBrowsingForPeers: %@", log: .default, type: .error, error.localizedDescription)
        // NSLog("%@", "didNotStartBrowsingForPeers: \(error)")
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        NSLog("%@", "✅ foundPeer: \(peerID)")
        let peerIDNameString = peerID.displayName
        if let discoveryInfoDict = info {
            os_log("✅ discoveryInfoDict:  %@", log: .default, type: .debug, discoveryInfoDict.description)
        }
        if peerIDNameString.contains(globalMasterSubstring) {
            os_log("✅ Found master! send invitation to it.", log: .default, type: .debug)
            self.serviceBrowser.invitePeer(peerID, to: self.mcSession, withContext: nil, timeout: self.invitingPeerTimeout)
        }
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        NSLog("%@", "❌ lostPeer: \(peerID)")
    }
}


extension SlavePage {
    private func popWhenMasterLeaves() {
        for p in self.connectedPeersObserver {
            if p.displayName.contains(globalMasterSubstring) {
                return
            }
        }
        // master not in the session, it means the sessions is over
        DispatchQueue.main.async { [unowned self] in
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    private func stopBrowsingWhenConnected() {
        self.serviceBrowser.stopBrowsingForPeers()
    }
}

extension SlavePage: MCSessionDelegate {
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        os_log("✅ currently session peers: %@", log: .default, type: .debug, self.mcSession.connectedPeers)
        // if master is not in the session, it means the sessions is over, and the slave should pop to main menu
        if self.connectedPeersObserver.count != self.mcSession.connectedPeers.count {
            self.connectedPeersObserver = self.mcSession.connectedPeers
        }
        switch state {
        case MCSessionState.connected:
            print("Connected: \(peerID.displayName)")
            self.stopBrowsingWhenConnected()
            // os_log("✅ current connected count: %@", log: .default, type: .debug, self.mcSession.connectedPeers.count)
        case MCSessionState.connecting:
            print("Connecting: \(peerID.displayName)")
        case MCSessionState.notConnected:
            print("Not Connected: \(peerID.displayName)")
        @unknown default:
            fatalError()
        }
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        if let recvdImage = UIImage(data: data) {
            self.ImagePieceDict[self.currentReceivedID] = recvdImage
            DispatchQueue.main.async { [unowned self] in
                // do something with the image
                self.imageView.fadeOut()
                self.imageView.image = recvdImage
                UIView.animate(withDuration: 0.5/*Animation Duration second*/,  delay: 0, options: .curveEaseOut, animations: {
                    self.imageView.fadeIn()
                }, completion: nil)
                // self.navigationItem.title = "Received image!"
            }
            return
        }
        if let recvdID = data.to(type: Int.self) {
            
            // first, check for videoID as highest priority
            if let url = globalVideoIDs[recvdID] {
                self.videoView.reset()
                self.videoView.configure(videoURL: url)
                self.videoView.play()
                DispatchQueue.main.async {
                    self.videoView.isHidden = false
                }
                return
            }
            
            // else, go with normal image process
            self.currentReceivedID = recvdID
            if let img = self.ImagePieceDict[recvdID] {
                DispatchQueue.main.async { [unowned self] in
                    self.imageView.fadeOut()
                    self.imageView.image = img
                    UIView.animate(withDuration: 0.5/*Animation Duration second*/,  delay: 0, options: .curveEaseOut, animations: {
                        self.imageView.fadeIn()
                    }, completion: nil)
                }
            }
        }
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        // do nothing here right now
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        // do nothing here right now
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        // do nothing here right now
    }
}


